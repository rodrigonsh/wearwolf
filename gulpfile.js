// wearWolf - GULPFILE
// Rodrigo Nishino <rodrigo.nsh@gmail.com>
// https://bitbucket.org/rodrigonsh/wearwolf

var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var fs = require("fs");
var replace = require("gulp-replace");

console.log("wearWolf");
console.log("Rodrigo Nishino <rodrigo.nsh@gmail.com>");
console.log("https://bitbucket.org/rodrigonsh/wearwolf");
console.log("=========================================\n\n");

var cssFiles = ['src/scss/app.scss', 'src/scss/wearwolf.scss'];


var loadFirebase = fs.readFileSync("src/html/load-firebase.html", "utf8");
var navContent = fs.readFileSync("src/html/nav.html", "utf8");
var initPage = fs.readFileSync("src/html/page-init.html", "utf8");
var loginPage = fs.readFileSync("src/html/page-login.html", "utf8");
var registerPage = fs.readFileSync("src/html/page-register.html", "utf8");
var editorPage = fs.readFileSync("src/html/page-editor.html", "utf8");
var publishPage = fs.readFileSync("src/html/page-publish.html", "utf8");
var uploadPage = fs.readFileSync("src/html/page-upload.html", "utf8");
var inboxPage = fs.readFileSync("src/html/page-inbox.html", "utf8");
var composePage = fs.readFileSync("src/html/page-compose.html", "utf8");
var contactTemplate = fs.readFileSync("src/html/template-contact.html", "utf8");
var sectionTemplate = fs.readFileSync("src/html/template-section.html", "utf8");
var messageListTemplate = fs.readFileSync("src/html/template-message-list.html", "utf8");



var appFiles = [
  'src/js/config.js',
  'src/js/alpha.js',
  'src/js/auth.js',
  'src/js/firebase.js',
  'src/js/headscroll.js',
  'src/js/google-fonts.js',
  'src/js/wolf.js',
  'src/js/set_youtube.js',
  'src/js/www-app.js'
  ];
  
var adminFiles = [
  'src/js/config.js',
  'src/js/alpha.js',
  'src/js/auth.js',
  'src/js/firebase.js',
  'src/js/headscroll.js',
  'src/js/wolf.js',
  'src/js/www-admin.js'
  ];
  
var uploadFiles = [
  'src/js/config.js',
  'src/js/alpha.js',
  'src/js/auth.js',
  'src/js/firebase.js',
  'src/js/headscroll.js',
  'src/js/wolf.js',
  'src/js/www-upload.js'
  ];
  
var inboxFiles = [
  'src/js/config.js',
  'src/js/alpha.js',
  'src/js/auth.js',
  'src/js/firebase.js',
  'src/js/headscroll.js',
  'src/js/wolf.js',
  'src/js/www-inbox.js'
  ];

gulp.task('html-snippets', function(cb)
{
  loadFirebase = fs.readFileSync("src/html/load-firebase.html", "utf8");
  navContent = fs.readFileSync("src/html/nav.html", "utf8");
  initPage = fs.readFileSync("src/html/page-init.html", "utf8");
  loginPage = fs.readFileSync("src/html/page-login.html", "utf8");
  registerPage = fs.readFileSync("src/html/page-register.html", "utf8");
  editorPage = fs.readFileSync("src/html/page-editor.html", "utf8");
  publishPage = fs.readFileSync("src/html/page-publish.html", "utf8");
  inboxPage = fs.readFileSync("src/html/page-inbox.html", "utf8");
  composePage = fs.readFileSync("src/html/page-compose.html", "utf8");
  contactTemplate = fs.readFileSync("src/html/template-contact.html", "utf8");
  sectionTemplate = fs.readFileSync("src/html/template-section.html", "utf8");
  messageListTemplate = fs.readFileSync("src/html/template-message-list.html", "utf8");
  cb();
})

gulp.task('index.html', function()
{
  return gulp.src("src/html/index.html")
    .pipe(replace("LOAD_FIREBASE", loadFirebase))
    .pipe(replace("NAV_HERE", navContent))
    .pipe(replace("PAGE_INIT_HERE", initPage))
    .pipe(replace("PAGE_LOGIN_HERE", loginPage))
    .pipe(replace("PAGE_REGISTER_HERE", registerPage))
    .pipe(replace("PAGE_EDITOR_HERE", editorPage))
    .pipe(replace("PAGE_PUBLISH_HERE", publishPage))
    .pipe(replace("TEMPLATE_CONTACT_HERE", contactTemplate))
    .pipe(replace("TEMPLATE_SECTION_HERE", sectionTemplate))
    .pipe(replace("SCRIPT_NAME_HERE", 'app.min.js'))
    .pipe(gulp.dest("public"))
})

gulp.task('admin.html', function()
{
  return gulp.src("src/html/admin.html")
    .pipe(replace("LOAD_FIREBASE", loadFirebase))
    .pipe(replace("NAV_HERE", navContent))
    .pipe(replace("PAGE_INIT_HERE", initPage))
    .pipe(replace("PAGE_LOGIN_HERE", loginPage))
    .pipe(replace("PAGE_REGISTER_HERE", registerPage))
    .pipe(replace("PAGE_EDITOR_HERE", editorPage))
    .pipe(replace("PAGE_PUBLISH_HERE", publishPage))
    .pipe(replace("SCRIPT_NAME_HERE", 'admin.min.js'))
    .pipe(gulp.dest("public"))
})

gulp.task('upload.html', function()
{
  return gulp.src("src/html/upload.html")
    .pipe(replace("LOAD_FIREBASE", loadFirebase))
    .pipe(replace("NAV_HERE", navContent))
    .pipe(replace("PAGE_INIT_HERE", initPage))
    .pipe(replace("PAGE_LOGIN_HERE", loginPage))
    .pipe(replace("PAGE_REGISTER_HERE", registerPage))
    .pipe(replace("PAGE_UPLOAD_HERE", uploadPage))
    .pipe(replace("SCRIPT_NAME_HERE", 'upload.min.js'))
    .pipe(gulp.dest("public"))
})

gulp.task('inbox.html', function()
{
  return gulp.src("src/html/inbox.html")
    .pipe(replace("LOAD_FIREBASE", loadFirebase))
    .pipe(replace("NAV_HERE", navContent))
    .pipe(replace("PAGE_INIT_HERE", initPage))
    .pipe(replace("PAGE_LOGIN_HERE", loginPage))
    .pipe(replace("PAGE_REGISTER_HERE", registerPage))
    .pipe(replace("PAGE_INBOX_HERE", inboxPage))
    .pipe(replace("PAGE_COMPOSE_HERE", composePage))
    .pipe(replace("TEMPLATE_MSG_LIST_HERE", messageListTemplate))
    .pipe(replace("SCRIPT_NAME_HERE", 'inbox.min.js'))
    .pipe(gulp.dest("public"))
})

gulp.task('css', function(){
  return gulp.src(cssFiles)
    .pipe(sass())
    .pipe(minifyCSS())
    .pipe(gulp.dest('public'))
});

gulp.task('app-js', function(){
  return gulp.src(appFiles)
    //.pipe(sourcemaps.init())
    .pipe(concat('app.min.js'))
    //.pipe(sourcemaps.write())
    .pipe(gulp.dest('public'))
});

gulp.task('admin-js', function(){
  return gulp.src(adminFiles)
    .pipe(sourcemaps.init())
    .pipe(concat('admin.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public'))
});

gulp.task('upload-js', function(){
  return gulp.src(uploadFiles)
    .pipe(sourcemaps.init())
    .pipe(concat('upload.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public'))
});

gulp.task('inbox-js', function(){
  return gulp.src(inboxFiles)
    .pipe(sourcemaps.init())
    .pipe(concat('inbox.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public'))
});

gulp.task('serve', function(done) {

  browserSync({
    server: "public",
    https: true,
    files: ["public/*.css", "public/*.js", "public/*.html"],
    online: false,
    open: "local",
    port: 9000,
  }, done);

});

gulp.task('default', [
  'html-snippets', 
  'index.html', 
  'admin.html', 
  'upload.html', 
  'inbox.html', 
  'serve', 
  'css', 
  'app-js', 
  'admin-js', 
  'upload-js',
  'inbox-js'
  ]);

gulp.watch("src/html/*", 
  [
    'html-snippets', 
    'index.html', 
    'admin.html',
    'upload.html',
    'inbox.html'
  ])
gulp.watch("src/js/*", ['app-js', 'admin-js', 'upload-js', 'inbox-js'])
gulp.watch("src/scss/*", ["css"])
