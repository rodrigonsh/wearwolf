// wearWolf - ADMIN
// Rodrigo Nishino <rodrigo.nsh@gmail.com>
// https://bitbucket.org/rodrigonsh/wearwolf

entry = "admin";
var container = document.getElementById("jsoneditor");
var options = {};
var editor = new JSONEditor(container, options);

loadDEV = true

addEventListener('siteDataReady', function()
{
  
  siteData = JSON.parse(localStorage.getItem(user.uid+'-DEV'))
  editor.set(siteData)     
  pageTo("editor");     
  
})

addEventListener('resetAction', function(ev)
{
  if (localStorage.getItem(user.uid) != null)
  {
    siteData = JSON.parse(localStorage.getItem(user.uid))
    localStorage.setItem(user.uid+"-DEV", JSON.stringify(siteData))
    emit("siteDataReady")
  }
  
})

addEventListener("saveAction", function(ev)
{
  siteData = editor.get()
  localStorage.setItem(user.uid+"-DEV", JSON.stringify(siteData))
  console.log("dispatch reload event")
  q("iframe").contentDocument.documentElement.dispatchEvent( new Event("reload") )
  
})

var $publishPage = q("#publish")
var $pubishError = q("#publish error strong");

addEventListener("publishAction", function(ev)
{
  
  console.log("publish", user.uid, siteData)
  
  pageTo("publish")
  
  $publishPage.setAttribute("processing", true)
  $publishPage.removeAttribute("failed")
  $publishPage.removeAttribute("ok")
  
  db.ref("sites/"+user.uid)
    .set(siteData, function(err)
    {
      if (err)
      {
        console.error(err)
        $publishPage.removeAttribute("processing")
        $publishPage.setAttribute("failed", true)
        $publishError.textContent = error
      }
      
      else
      {
        console.log("ok")
        $publishPage.setAttribute("ok", true)
        $publishPage.removeAttribute("processing")
      }
    })
    
  var blob = new Blob(
    [q("iframe#live-preview").contentDocument.documentElement.outerHTML],
    { type: 'plain/text', endings: 'native' }
    );
    
  storage.ref("sites/"+user.uid).put(blob)
  
})

setTimeout(function()
{
  q("iframe").contentDocument.documentElement.dispatchEvent( new Event("reload") )
  emit("siteDataReady")
}, 1000)

saveBtn = create("button", "SALVA & PREVIEW")
saveBtn.setAttribute("emit", "save")
saveBtn.setAttribute("logged", true)

resetBtn = create("button", "RESETA DEV")
resetBtn.setAttribute("emit", "reset")
resetBtn.setAttribute("logged", true)

publishBtn = create("button", "PUBLICAR SITE")
publishBtn.setAttribute("emit", "publish")
publishBtn.setAttribute("logged", true)


$nav.querySelector("group").appendChild(resetBtn)
$nav.querySelector("group").appendChild(publishBtn)
$nav.querySelector("group").appendChild(saveBtn)


