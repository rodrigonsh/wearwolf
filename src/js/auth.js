var user = null
var db = null
var auth = null
var messaging = null
var $loginPage = q("#login")
var $loginError = $loginPage.querySelector("error strong")
var $loginForm = q("#login form")

var userInbox = null
var userInboxRef = null

$loginForm.addEventListener("submit", function(ev)
{
  
  ev.preventDefault()
  ev.stopPropagation()
  
  $loginPage.removeAttribute("failed")
  $loginPage.setAttribute("processing", true)
  
  var email = $loginForm.querySelector("[type=email]").value
  var password = $loginForm.querySelector("[type=password]").value
  
  auth.signInWithEmailAndPassword(email, password).catch(function(error){
    
    console.error("AUTH ERROR", error)
    
    $loginPage.removeAttribute("processing")
    $loginPage.setAttribute("failed", true)
    
    $loginError.textContent = error.message
    
  });
  
})

var $registerPage = q("#register")
var $registerForm = q("#register form")
var $registerError = q("#register error strong")

$registerForm.addEventListener("submit", function(ev)
{
  ev.preventDefault()
  ev.stopPropagation()
  
  var email = $registerForm.querySelector("[type=email]").value
  var password = $registerForm.querySelector("[type=password]").value
  var confirmation = $registerForm.querySelector("[name=confirmation]").value
  
  console.log("register", email, password, confirmation)
  
  if (password != confirmation)
  {
    $registerPage.setAttribute("failed", true)
    $registerError.textContent = "Senha de confirmação não confere!"
    return
  }
  
  $registerPage.removeAttribute("failed")
  $registerPage.setAttribute("processing", true)
  
  auth.createUserWithEmailAndPassword(email, password)
      .catch(function(error){
    
    console.error("AUTH ERROR", error)
    
    $registerPage.removeAttribute("processing")
    $registerPage.setAttribute("failed", true)
    
    $registerError.textContent = error.message
    
  });
  
})

addEventListener("userReady", function(ev)
{
  
  
  if ( entry == "index" && loadDEV ) return;
  
  $body.classList.add("user-online")
  $nav.querySelector("#username").textContent = user.email.replace('@', ' @')
  q("#loggedUserAvatar").src = user.photoURL
  
  if ( window.innerWidth > 800 )
  {
    console.log('a wild navbar appears');
    setTimeout(function(){$shell.classList.add("nav")}, 300);     
  }
  
  userInboxRef = db.ref("inbox/"+user.uid)
  
  userInboxRef.on("value", function(snap)
  {
    
    console.error("unserInboxRef on Value", snap.val());
    
    if (snap.val() == null) return
    
    userInbox = JSON.parse(localStorage.getItem("inbox-"+user.uid))
    if (userInbox == null)
    {
      userInbox = {}
      localStorage.setItem('inbox-'+user.uid, "{}")
    }
    
    newMessages = snap.val()
    userInbox = Object.assign(userInbox, newMessages)
    
    localStorage.setItem("inbox-"+user.uid, JSON.stringify(userInbox))
    
    //db.ref("inbox/"+user.uid).set(null);    
    emit("userInboxReady")
    
  })
  
  if(loadDEV)
  {
    
    siteData = JSON.parse(localStorage.getItem(user.uid+"-DEV"))
    console.log("reloading dev", siteData)
      
    if(siteData == null)
    {
    
      console.log("loading exampleSiteData.json...");
      var http = fetch("exampleSiteData.json")
        .then(function(response){ return response.json() })
        .then(function(response)
        {
          siteData = response;
          localStorage.setItem(user.uid+"-DEV", JSON.stringify(siteData))
          emit("siteDataReady". siteData)  
        });
    
    } else {
      emit("siteDataReady", siteData)  
    }
    
    
  } else {
    siteData = JSON.parse(localStorage.getItem(user.uid))
    emit("siteDataReady", siteData)
  }
    
  
  
  messaging.requestPermission().then(function()
  {
    console.log('Notification permission granted.');
    return messaging.getToken()
  })
  .then(function(tk){
    
    console.log("u have de token", tk)
    
    tokens = JSON.parse(localStorage.getItem(user.uid+'_tokens'))
    
    if (tokens == null) tokens = []
    
    console.log('tokens.indexOf(tk)', tokens.indexOf(tk))
    
    if (tokens.indexOf(tk) == -1)
    {
      
      
      addToken(tk)
        .then(function(res)
        {
          if (res.data.ok == true)
          {
            tokens.push(tk)
            localStorage.setItem(user.uid+'_tokens', JSON.stringify(tokens))
          }
        })
        .catch(console.error)
      
    } else { console.log('Token já existe') }
    
  })
  .catch( console.error )
  
})

addEventListener("userNull", function(ev)
{
  $body.classList.remove("user-online")
  let skipLogin = localStorage.getItem('skipLogin')
  
  console.log("hum", entry, skipLogin)    
  
  if ( 
    
    ( entry == 'index' ) && 
    ( skipLogin == 'true' )
  )
  {
    
    if (publicHosts) pageTo("public")
    else pageTo("landing")
    
  } else if ( h == www ){ 
    console.log("gotta show some login")
    pageTo("login")
  }
}) 

addEventListener("logoutAction", function()
{
  auth.signOut()
  if ( entry != index )
  {
    window.location = "index.html"
  }
})

addEventListener('skipLoginAction', function(ev)
{
  localStorage.setItem('skipLogin', true)
  emit('pullPageAction')
})
