var addToken = null
var sendInbox = null



function gotSiteData(s)
{
  console.log('gotSiteData', s.val())
  
  if( s.val() != null )
  {
    
    if (loadDEV)
    {
      console.log("whatever")
    }
    
    else
    {
      console.log('gotSiteData for',s.key , siteData)
      siteData = s.val()
      localStorage.setItem(s.key, JSON.stringify(siteData))
      emit("siteDataReady")
    }
    
  }
  
  else
  {
    if (entry == 'index') pageTo("landing")
    if (entry == 'admin') pageTo("editor")
    if (entry == 'inbox') pageTo("inbox")
    
    console.error(
      "Este domínio não está configurado, contacte-nos",
      "https://wearwolf-web.firebaseapp.com"
      )
  }
  
}

document.addEventListener('DOMContentLoaded', function()
{
  // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
  // // The Firebase SDK is initialized and available here!
  //
  // firebase.auth().onAuthStateChanged(user => { });
  // firebase.database().ref('/path/to/ref').on('value', snapshot => { });
  // firebase.messaging().requestPermission().then(() => { });
  // firebase.storage().ref('/path/to/ref').getDownloadURL().then(() => { });
  //
  // // 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥
  
  if ( undefined == firebase )  
  {
    console.error("no firebase");
    return;
  }

  try
  {
    
    let app = firebase.app();
    
    db = firebase.database()
    auth = firebase.auth()
    functions = firebase.functions()
    
    if (entry == 'upload' || entry == 'admin')
    {
      storage = firebase.storage()
    }
    
    messaging = firebase.messaging();
    messaging.usePublicVapidKey(publicVapidKey)
    
    messaging.onMessage(function(payload)
    {
      alert(payload.notification.title);
      console.log("ONMESSAGE", payload)
    })
    
    addToken = functions.httpsCallable('addToken')
    sendInbox = functions.httpsCallable('sendInbox')
    
    let publicRef = db.ref("public");
    
    if (h != "localhost_9000" && h != www)
    {
    
      emit("pro")
      let hostRef = db.ref("hosts/"+h)
            
      hostRef.on("value", function(snap)
      {
        if (snap.val() != null)
        {
          console.log("hostRef on value")
          UID = snap.val();
          emit("loadUID", UID)
        }
        
      })
      
    } else {
    
      publicRef.on("value", function(s)
      {
        emit("free")
        console.log("publicRef on value")
        publicHosts = s.val();
        localStorage.setItem("publicHosts", JSON.stringify(publicHosts))
        emit("publicHostsReady");
      })  
      
    }
    
    
    
    
    auth.onAuthStateChanged(function(u)
    {
      user = u
      if (user != null)
      {
        UID = user.uid
        emit("userReady",UID);
        emit("loadUID", UID);
      }
      else emit("userNull")
    })
      
  
  } 
  
  catch (e)
  {
    console.error(e)
    alert("Ocorreu um erro! COD#603B02")
    
    siteData.styles.background = "#393939"
    siteData.styles.color = "#2DF341",
    siteData.styles["--contentFont"] = "monospace",
    siteData.styles["--pitchFontFamily"] = "monospace"
              
    //configure(document.documentElement, siteData)
    
  }
  
});


addEventListener("loadUID", function(ev)
{
  if (UID == null) return;
  
  
  
  if (entry != "index" && UID != user.uid)
  {
    window.location = endPoint+"index.html#"+UID
    return;
  }
  
  $shell.classList.remove("nav")
  
  if(q("#landing")) q("#landing").scrollTop = 0
  
  pageTo("init")  
    
  json = localStorage.getItem(UID)
  if (loadDEV)
  {
    json = localStorage.getItem(UID+"-DEV")
    console.log("carregando DEV", json)
  }
  
  try
  {
    if (json != null)
    {
      siteData = JSON.parse(json)
      emit('siteDataReady', siteData)
    }
    
    db.ref("sites/"+UID).on("value", gotSiteData)
  }
  
  catch (error){ console.error('cocozinho', error) }
  
})


