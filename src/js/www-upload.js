// wearWolf - ADMIN
// Rodrigo Nishino <rodrigo.nsh@gmail.com>
// https://bitbucket.org/rodrigonsh/wearwolf

entry = "upload"
loadDEV = true
var $uploadPage = q("#upload")
var $uploadError = q("#upload error strong")
var toUpload = 0
var uploaded = 0

function upload(k, file, evName)
{
  toUpload++
  var storageRef = storage.ref(k)
  storageRef.put(file).then(function(){ emit(evName, storageRef) })
}

function uploadSuccessful(name)
{
  uploaded++
  if(toUpload == uploaded)
  {
    window.location = "admin.html"
  }
  
  var res = $uploadPage.querySelector("results")
  res.appendChild( create('h3', name) )
  
}

addEventListener('siteDataReady', function()
{
 
  $sections = q("#sections")
  $sections.innerHTML = ""
  
  for ( k in siteData )
  {
    if ( k == 'about' || k == 'styles' ) continue
    
    let group = create("group")
    group.setAttribute('row', 'row')
    
    let label = create('label', k)
    
    let input = create('input')
    input.setAttribute('name', k)
    input.setAttribute('type', 'file')
    
    group.appendChild(label)
    group.appendChild(input)
    
    $sections.appendChild(group)
    
  }
  
  pageTo("upload");     
  $uploadPage.removeAttribute("processing")
  
})

function saveSiteData()
{
  localStorage.setItem(user.uid+"-DEV", JSON.stringify(siteData))
}

addEventListener("uploadAction", function(ev)
{
  
  console.log("upload", user.uid, siteData)
  
  $uploadPage.setAttribute("processing", true)
  $uploadPage.removeAttribute("failed")
  $uploadPage.removeAttribute("ok")
  
  // fire storage
  
  var logoFile = $uploadPage.querySelector('[name=logo]').files[0]
  var faviconFile = $uploadPage.querySelector('[name=favicon]').files[0]
  var avatarFile = $uploadPage.querySelector('[name=avatar]').files[0]
  var audioFile = $uploadPage.querySelector('[name=audio]').files[0]
  
  toUpload = 0
  uploaded = 0
  
  if (logoFile)
  {
    upload("logos/"+user.uid, logoFile, 'logoUploaded')
  }
  
  if (faviconFile)
  {
    upload("favicons/"+user.uid, faviconFile, 'faviconUploaded')
  }
  
  if (avatarFile)
  {
    upload("avatars/"+user.uid, avatarFile, 'avatarUploaded')
  }
  
  if (audioFile)
  {
    upload("audios/"+user.uid, audioFile, 'audioUploaded')
  }
  
  $sections = q("#upload #sections")
  $groups = $sections.querySelectorAll("input")
  
  for(var i=0; i<$groups.length; i++)
  {
    if($groups[i].files[0])
    {
      upload('users/'+user.uid+"/"+i, $groups[i].files[0], 'backgroundUploaded')
    }
    console.log(i, $groups[i].files[0])
  }
  
})

addEventListener('logoUploaded', function(ev)
{
  
  ev.originalEvent.getDownloadURL().then(function(url)
  {
    siteData.about.logoURL = url
    saveSiteData()
    uploadSuccessful("Logo atualizada")
    
  })
  
})

addEventListener('faviconUploaded', function(ev)
{
  
  ev.originalEvent.getDownloadURL().then(function(url)
  {
    siteData.about.iconURL = url
    saveSiteData()
    uploadSuccessful("Ícone atualizado")
  })
  
})

addEventListener('audioUploaded', function(ev)
{
  
  ev.originalEvent.getDownloadURL().then(function(url)
  {
    siteData.about.audioURL = url
    saveSiteData()
    uploadSuccessful("Áudio atualizado")
  })
  
})

addEventListener('avatarUploaded', function(ev)
{
  
  ev.originalEvent.getDownloadURL().then(function(url)
  {
    siteData.about.avatarURL = url
    saveSiteData()
    user.updateProfile({photoURL: url})
        .then( function(){
          uploadSuccessful("Avatar atualizado")
          })
        .catch(console.error)
    
  })
  
})

addEventListener('backgroundUploaded', function(ev)
{
  var loc = ev.originalEvent.location
  ev.originalEvent.getDownloadURL().then(function(url)
  {
    
    
    var idx = parseInt(loc.path.split("/").pop())
    
    $sections = q("#upload #sections")
    $groups = $sections.querySelectorAll("input")
  
    var k = $groups[idx].getAttribute("name")
    
    siteData[k].image = url
    saveSiteData()
    uploadSuccessful(k+" atualizado")
    
  })
})
