// wearWolf - INBOX
// Rodrigo Nishino <rodrigo.nsh@gmail.com>
// https://bitbucket.org/rodrigonsh/wearwolf

entry = 'inbox';

var selectedInboxUser = null

var $inboxUnread = null
var $inboxRead = null
var $messages = null

addEventListener("userInboxReady", function(ev)
{
  
  $inboxUnread = q("page#inbox #inboxUnread");
  $inboxRead = q("page#inbox #inboxRead");
  
  $inboxUnread.innerHTML = ""
  $inboxRead.innerHTML = ""
  
  for( inboxUID in userInbox )
  {
    
    unread = 0
    
    for( ts in userInbox[inboxUID] )
    {
      
      if ( !userInbox[inboxUID][ts].read ) unread++
      
    }
    
    
    var li = create("li", inboxUID.substr(0, 7));
    li.setAttribute("emit", "showMessages");    
    li.dataset.uid = inboxUID
    
    
    if ( unread > 0 ) 
    {
      li.classList.add("unread");
      $inboxUnread.appendChild(li);
    }
    
    else
    {
      $inboxRead.appendChild(li);
    }
    
  }
  
  pageTo("inbox")
  
})

addEventListener("showMessagesAction", function(ev)
{
  
  
  selectedInboxUser = userInbox[ ev.originalEvent.target.dataset.uid ]
  console.log('showMessages', ev.originalEvent.target.dataset.uid, selectedInboxUser);
  
  $messages = q("page#inbox #messages")
  $messages.innerHTML = ""
  
  var t = q("template#message-list")
  
  for(ts in selectedInboxUser)
  {
    
    var $frag = document.importNode(t.content, true);
    var $msg = $frag.querySelector("message")
    
    $msg.querySelector('pre').textContent = selectedInboxUser[ts].message
    $msg.querySelector('[name]').textContent = selectedInboxUser[ts].name
    $msg.querySelector('[email]').textContent = selectedInboxUser[ts].email
    
    $messages.appendChild($frag)
    
  }
  
})
