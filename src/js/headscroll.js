// headScroll
// Rodrigo Nishino <rodrigo.nsh@gmail.com>

$doc.lastScrollTop = 0;
$doc.last_dir = "▲";
$doc.cur_dir = null;

var elastic = false;

function headScroll(ev)
{
  
  //$('#debugsOnTheTable').html( $window.scrollTop() );
  
  // iphonefix
  if ( elastic )
  {
      $body.classList.add("up")
      $body.classList.remove("down");
      return;
  }

  if ( ev.target.scrollTop < 0 )
  {
      elastic = true;
      setTimeout( function(){ elastic = false; }, 2000 );
      return;
  }

  if( ev.target.scrollTop < 300 )
  {
      if ( ev.target.scrollTop > 50 ) $body.classList.add('scrolling')
      else $body.classList.remove('scrolling')
  }

  if ( ev.target.scrollTop > $doc.lastScrollTop )
  {
      $doc.cur_dir  = "▼"

      if ($doc.cur_dir != $doc.last_dir)
      {
          $doc.last_dir = $doc.cur_dir;
          $body.classList.add("down")
          $body.classList.remove("up")
      }

  }
  else
  {
      $doc.cur_dir = "▲"

      if ($doc.cur_dir != $doc.last_dir)
      {
          $doc.last_dir = $doc.cur_dir;
          $body.classList.add("up")
          $body.classList.remove("down")
      }
  }

  $doc.lastScrollTop = ev.target.scrollTop

}

allPages = document.querySelectorAll("page")
for( var i=0; i < allPages.length; i++ )
{
  allPages[i].addEventListener("scroll", headScroll )
}


