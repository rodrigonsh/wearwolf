// wearWolf - FRONT
// Rodrigo Nishino <rodrigo.nsh@gmail.com>
// https://bitbucket.org/rodrigonsh/wearwolf

entry = 'index';
var currentAbout;

function setAbout(v)
{
  
  var $header = q("header")
  var $headerIcon = $header.querySelector("#headerIcon")
  var $audioBtn = $header.querySelector("button.audio")
  
  if ( "iconURL" in v )
  {
    
    if ($headerIcon == null)
    {
      var $headerIcon = create("img");
      let $span = $header.querySelector("span")
      $headerIcon.id = "headerIcon"
      $header.insertBefore($headerIcon, $span)
    }

    $headerIcon.src = v.iconURL;
    
  }
  
  else
  {
    if ($headerIcon != null) $headerIcon.delete()
  }
  
  
  if ( "audioURL" in v ) $audioBtn.setAttribute("audioURL", v.audioURL)
  else $audioBtn.removeAttribute("audioURL")
  
  
  if ( "name" in v )
  {
    document.title = v.name
    q("header span").textContent = v.shortName
  }
  
}



function setStyles(styles)
{
  
  // TODO: clearStyles
  
  var el = document.documentElement
  var fontKeys = ["--headerFont", "--pitchFont", "--contentFont"]
    
  light = tinycolor("#EEEEEE")
  dark = tinycolor("#2D2D2D")
  
  for( k in styles)
  {
    
    let v = styles[k]
    
    //console.log(k, v)
    
    if ( fontKeys.indexOf(k) > -1 )
    {
      
      if (  v != "sans-serif" && 
            v != "serif" && 
            googleFonts.indexOf(k) == -1 )
      {
        googleFonts.push( v )
        setTimeout(()=>emit('updateGoogleFonts'), 300)
      }
      
      el.style.setProperty(k, v);
      
    }
     
    else
    {
      
      el.style.setProperty(k, v);
      
      if (k == '--light')
      {
        //console.log(k, v)
        light = tinycolor(v)
      }
      
      if (k == '--dark')
      {
        //console.log(k, v)
        dark = tinycolor(v)
      }
      
    }
    
  }
  
  //console.log('final light', light.toHexString())
  //console.log('final dark', dark.toHexString())
  
  el.style.setProperty("--light", light.toHexString())
  el.style.setProperty("--lighter", light.lighten(10).toHexString())
  
  el.style.setProperty("--dark", dark.toHexString())
  el.style.setProperty("--darker", dark.darken(10).toHexString())
 
  let neutral = tinycolor.mix(light, dark)
  
  el.style.setProperty("--neutral", neutral.toString())
  
  //console.log("light", light.toHexString())
  //console.log("lighter", light.lighten(10).toHexString())
  //console.log("dark", dark.toHexString())
  //console.log("darker", dark.darken(10).toHexString())
  
}

addEventListener("set_blendmode", function(ev)
{
  ev.container.setAttribute("blendmode", ev.value)
})

addEventListener("set_image", function(ev)
{
  let url = 'url('+ev.value+')'
  ev.container.style.setProperty('background-image', url)
})


addEventListener("set_emit", function(ev)
{
  ev.container.setAttribute("emit", ev.value)
})


addEventListener("set_link", function(ev)
{
  ev.container.setAttribute("link", ev.value)
})


addEventListener("set_call", function(ev)
{
  if ( ev.container.nodeName == "BUTTON" )
  {
    ev.container.textContent = ev.value
  } 
  
  else
  {
    
    let btn = create("button")
    btn.textContent = ev.value
    
    let buttons = ev.container.querySelector("group")
    
    if (buttons == null) ev.container.appendChild(btn)
    else buttons.appendChild(btn)
    
  }
})


addEventListener("set_pitch", function(ev)
{
  if (ev.container == null) return;
  
  let pitch =  ev.container.querySelector("pitch")
  let span = create("span")
  span.textContent = ev.value
  pitch.appendChild(span)
  
})


addEventListener("set_selector", function(ev)
{
  
  if (ev.container == null) return;
  
  let selParts = ev.value.split(".")
  
  for( var i = 0; i < selParts.length; i++ )
  {
    if (selParts[i] == "") continue;
    else
    {
      if(selParts[i][0] == "#")
      {
        ev.container.setAttribute("id", selParts[i].substr(1))
      } else
      {
        ev.container.classList.add(selParts[i])
      }
    }
  }
  
})

addEventListener("set_dark", function(ev)
{
  //console.log("setting dark", ev)
  if (ev.value == true) ev.container.classList.add("inverted")
  else ev.container.classList.remove("inverted")
})


addEventListener("set_arguments", function(ev)
{
  let data = ev.value;
  let keys = Object.keys(data)
  
  let group = create('group')
  
  for( var i=0; i < keys.length; i++ )
  {
    let arg = create("argument")
    let call = null
    
    if ('call' in data[keys[i]])
    {
      call = data[keys[i]].call
      //console.log(call)
      delete data[keys[i]].call
      //console.log(call)
    }
    
    arg = configure(arg, data[keys[i]])
    
    if ('argument' in data[keys[i]])
    {
      let title = create("p")
      title.textContent = data[keys[i]].argument
      arg.appendChild(title)
    }
    
    if ('subtitle' in data[keys[i]])
    {
      let stitle = create("small")
      stitle.textContent = data[keys[i]].subtitle
      arg.appendChild(stitle)
    }
    
    if (call != null)
    {
      let button = create("button")
      button.textContent = call
      arg.appendChild(button)
      call = null
    }
    
    group.appendChild(arg)
    
  }
  
  ev.container.appendChild(group)
  
})


addEventListener("set_icon", function(ev)
{
  
  let icon = create("i")
  icon.className = "i-"+ev.value
  ev.container.appendChild(icon)
  
})


addEventListener("set_meme", function(ev)
{

  ev.container.classList.add("meme")
  
  let kicker = create("kicker")
  kicker.textContent = ev.value
  ev.container.appendChild(kicker)
  
})


addEventListener("set_buttons", function(ev)
{
  
  var group = null
  
  if ( ev.container.querySelector("group") )
  {
    group = ev.container.querySelector("group")
    
  }
  
  else
  {
    group = create("group")
    ev.container.appendChild(group)
  }
  
  let bkeys = Object.keys(ev.value)
  for(var i=0; i < bkeys.length; i++)
  {
    let button = create("button")
    button = configure(button, ev.value[bkeys[i]])
    group.appendChild( button )
  }
  
})


addEventListener("set_contact", function(ev)
{
  
  var t = q("template#contact")
  var $frag = document.importNode(t.content, true);
  
  ev.container.appendChild($frag)
  configure($frag.querySelector("group"), ev.value)
    
  var $avatar = ev.container.querySelector("img.avatar")
  var $name = ev.container.querySelector("h3")
  var $addr = ev.container.querySelector("[addr]")
  var $phone = ev.container.querySelector("[phone]")
  var $social = ev.container.querySelector("social")
  
  //console.log('set contact', ev.config)
  
  $avatar.src = ev.config.about.avatarURL
  $name.textContent = ev.value.sales
  $addr.textContent = ev.value.addr
  
  var iNumber = ev.value.mobile
  iNumber = replace(" ", "-", iNumber)
       
  $phone.href = "tel:"+iNumber
  $phone.querySelector("span.n").textContent = ev.value.mobile
  
  
  if ( 'social' in ev.value )
  {
    
    for( k in ev.value.social )
    {
      var b = create("i")
      b.setAttribute("link", ev.value.social[k])
      b.className = "i-"+k
      $social.appendChild(b)
    }
  
  }
  
  $nameInput = ev.container.querySelector("[name=name]");
  $emailInput = ev.container.querySelector("[type=email]");
  
  $nameInput.value = user.displayName
  $emailInput.value = user.email
  
  ev.container.querySelector("form").addEventListener("submit", sendMessage)

})

addEventListener("set_prices", function(ev)
{
  
  var data = ev.value
  var group = create("group")
  
  var plans = Object.keys( data )
  
  for(var i = 0; i < plans.length; i++)
  {
    var plan = create("card")
    
    let call = null
    
    if ('call' in data[plans[i]])
    {
      call = data[plans[i]].call
      delete data[plans[i]].call
    }
    
    
    configure(plan, ev.value[plans[i]])
    
    if (call != null)
    {
      let button = create("button")
      button.textContent = call
      plan.appendChild(button)
      call = null
    }
    
    group.appendChild( plan )
  }
  
  ev.container.appendChild(group)
  
})


addEventListener("set_text", function(ev)
{
  var txt = create("p", ev.value);
  ev.container.appendChild(txt)
})


addEventListener("set_price", function(ev)
{
  
  var $price = create("price")
  var $val = create("price_val")
  
  // TODO: internacionalizar esse ponto ae
  var priceParts =  ev.value.toString().split(" ")
  var valueParts =  priceParts[0].split(".")
  
  var valueParts =  ev.value.toString().split(".")
  if (priceParts.length == 2)
  {
    var $coin = create("price_coin")
    $coin.textContent = priceParts[0]
    $price.appendChild($coin)
    var valueParts =  priceParts[1].split(".")
  }
  
  $val.textContent = valueParts[0]
  
  if ( ev.value == 0 )
  {
    $val.textContent = "Free"
    $val.setAttribute("free", true)
  }
  
  $price.appendChild($val)
  
  if ( valueParts.length == 2 )
  {
    $cents = create("price_cents")
    $cents.textContent = valueParts[1]
    $price.appendChild($cents)
  }
  
  
  ev.container.appendChild($price)        
  
})


addEventListener("set_priceSubscription", function(ev)
{
  var $subs = create("price_subscription")
  $subs.textContent = ev.value
  ev.container.appendChild($subs)
})


addEventListener("set_plan", function(ev)
{
  var $title = create("h3")
  $title.textContent = ev.value
  ev.container.appendChild($title)
})


addEventListener("set_logo", function(ev)
{
  
  //console.log("setting logo", ev.about)
  
  let $text = ev.container.querySelector("p")
  
  let $logo = create("img")
  $logo.setAttribute("responsive", true)
  $logo.src = ev.about.logoURL
  
  ev.container.insertBefore($logo, $text)
  
})


addEventListener("set_features", function(ev)
{
  
  var keys = Object.keys(ev.value)
  for( var i = 0; i < keys.length; i++ )
  {
    var $feature = create("feature")
    configure($feature, ev.value[keys[i]])
    $feature.classList.add(ev.value[keys[i]].icon)
    ev.container.appendChild($feature)
  }
  
})


function configure(container, config, about)
{
  
  let keys = Object.keys(config)
  
  for(var i=0; i<keys.length; i++)
  {
    
    let parts = keys[i].split("_")
    if (parts.length == 1) { evName = "set_"+keys[i] }
    else { evName = "set_"+parts[ parts.length-1 ] }    
    
    let ev = new Event(evName)
    ev.container = container
    ev.value = config[keys[i]]
    ev.config = config
    ev.about = about
    dispatchEvent(ev)
    //console.log(evName, ev.value)
  
  }
  
  return container;
  
}

addEventListener('siteDataReady', function()
{
  console.log("siteDataReady", siteData)
  
  let manifest = "https://us-central1-wearwolf-web.cloudfunctions.net/manifest?uid="+UID
  $manifest = q("#linkManifest").setAttribute("href", manifest)
  
  setAbout(siteData.about)
  setStyles(siteData.styles)
     
  let $page = q("page#landing")
  $page.innerHTML = ""
  
  var keys = Object.keys( siteData )
  
  t = q("template#section")
  
  for( var i=0; i < keys.length; i++ )
  {
    if(keys[i] == "about" || keys[i] == "styles") continue;
    
    var $frag = document.importNode(t.content, true);
    var $section = $frag.querySelector("section")
    
    $section = configure( $section, siteData[keys[i]], siteData.about )
    $page.appendChild( $section )
    
    let key = keys[i].split("_").pop()
    let evName = "set_"+key
    
    $section.classList.add(key)
    $section.classList.add(keys[i])
    
    let ev = new Event(evName)
    ev.container = $section
    ev.value = siteData[keys[i]]
    ev.config = siteData
    ev.about = siteData.about
    dispatchEvent(ev)
    
  }
  
  pageTo("landing")
  $body.classList.remove("down")
  
})  
  
addEventListener('publicHostsReady', function()
{
  
  var UIDs = Object.keys(publicHosts)
  
  console.log("ON publicHostsReady", UIDs)
  
  var $public = q("page#public")
  $public.innerHTML = ""
  var t = q("template#section")
    
  if (showIndex)
  {
    q("header span").textContent = "wearWolf#index"
    
    $public.innerHTML = ""
    var $frag = document.importNode(t.content, true);
    var $section = $frag.querySelector("section")
    
    
    $section.querySelector("pitch").textContent = "Bem vindo"
    $section.classList.add("inverted")
    
    var $text = create("p")
    var $logo = create("img")
    
    $text.textContent = "Esta é a lista de sites públicos"
    
    $logo.src = "img/logo.png"
    $logo.setAttribute("responsive", true)
    $section.appendChild($logo)
    $section.appendChild($text)
    $public.appendChild($section)
  }
  
  for( var i=0; i < UIDs.length; i++ )
  {
    if ( publicHosts[ UIDs[i] ] == false ) continue;
    
    /*$li = create("li")
    $li.setAttribute("link", UIDs[i]);
    $li.textContent = publicHosts[ UIDs[i] ].about.name
    
    $ul.appendChild($li)*/
    
    var $frag = document.importNode(t.content, true);
    var $section = $frag.querySelector("section")
      
    let about = publicHosts[ UIDs[i] ].about
    let hero = publicHosts[ UIDs[i] ].hero
    configure($section, hero, about)
            
    $section.setAttribute("link", UIDs[i])
    $public.appendChild($section)

  }
  
  if (showIndex)
  {
    pageTo("public")
    showIndex = false;
  }
  
})    

addEventListener("userInboxReady", function(ev)
{
  
  if (userInbox == null) return
  
  var keys = qObject.keys(userInbox) 
  q("nav button#inbox span").textContent = keys.length
  
  alert("Nova mensagem");
  
  if ( window.innerWidth > 800 )
  {
    setTimeout(function()
    {
      $body.classList.add('nav');
    }, 2000);
  } 
  
  
})


if(window.location.hash)
{
  console.log("look dis", window.location.hash.substr(1))
  UID = window.location.hash.substr(1)
  emit("loadUID", window.location.hash.substr(1))
  } else {


  siteData = JSON.parse(localStorage.getItem(h))
  publicHosts = localStorage.getItem('publicHosts')

  if ( h == "localhost_9000" || h == www )
  {
    showIndex = true
        
  } else {
      
    
    if( siteData != null ) emit("siteDataReady", siteData)
     
      
  }




  if (publicHosts != null)
  {
    publicHosts = JSON.parse(publicHosts)
    console.log("publicHosts from CACHED")
    emit("publicHostsReady", publicHosts)
  }
  
}

function sendMessage(ev)
{
  
  ev.stopPropagation();
  ev.preventDefault();
  
  ts = new Date().valueOf()
  let msg = {}
  var target = ev.target
  
  target.removeAttribute("ok")
  target.removeAttribute("failed")
  target.setAttribute("processing", true)
  
  msg.to = UID
  msg.from = user.uid
  msg.read = false
  msg.name = target.querySelector("[name=name]").value
  msg.email = target.querySelector("[type=email]").value
  msg.message = target.querySelector("textarea").value
  
  let subscribe = target.querySelector("[name=subscribe]:checked")
  if (subscribe != null)
  {
    msg.subscribe = true
  }  
  
  ts = new Date().valueOf()
  
  var path = `/inbox/${UID}/${user.uid}/${ts}`
  
  console.log('contact', path, msg);
  
  db.ref(path)
  .set(msg, function(err)
  {
    
    if (err != null) 
    {
      
      target.removeAttribute("processing")
      target.setAttribute("failed", true)
      
      target.querySelector("error strong").textContent = err
      return
    }
    
    target.removeAttribute("processing")  
    target.setAttribute("ok", true)  
    
  })
  
  sendInbox(msg) 
  
}

addEventListener("logAction", function(ev)
{
  console.log("logAction", ev)
})


addEventListener("playAction", function(ev)
{
  var target = ev.originalEvent.target
  var audioURL = target.getAttribute("audiourl")
  target.setAttribute("emit", "stop")
  $globalAudio.src = audioURL
  $globalAudio.play()
  
  if (target.querySelector("i"))
  {
    target.querySelector("i").className = "i-stop"
  }
  
})

addEventListener("stopAction", function(ev)
{
  var target = ev.originalEvent.target
  $globalAudio.src = ""
  target.setAttribute("emit", "play")
  
  if (target.querySelector("i"))
  {
    target.querySelector("i").className = "i-play"
  }
})

document.documentElement.addEventListener("reload", function(ev)
{
  
  //console.log("RELOAD listener running", user)
  loadDEV = true
  
  if (user == null)
  {
    //console.log("agendando reload")
    setTimeout(function(){
      document.documentElement.dispatchEvent( new Event("reload") )
    }, 1000)
    
    return
  }
  
  siteData = JSON.parse(localStorage.getItem(user.uid+"-DEV"))
  emit("siteDataReady")
  
})

addEventListener("loadUIDAction", function(ev)
{
  UID = ev.originalEvent.target.dataset.uid
})

addEventListener("loadPublicAction", function(ev)
{
  showIndex = true;
  emit("publicHostsReady", publicHosts)
  pageTo("public")
})



addEventListener("userInboxReady", function(ev)
{
  
  let unread = 0
  
  for(senderUID in userInbox)
  {
    
    for( ts in userInbox[senderUID] )
    {
      
      console.log("userInbox", senderUID, ts)
      if ( !userInbox[senderUID][ts].read ) unread++
    }
    
  }
  
  if( unread > 0 )
  {
    q("nav #btnInbox span").textContent = unread;
  }
  else
  {
     q("nav #btnInbox span").textContent = "";
  }
  
})
