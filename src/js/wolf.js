light = tinycolor("#EEEEEE")
dark = tinycolor("#161616")
document.documentElement.style.setProperty("--light", light.toHexString())
document.documentElement.style.setProperty("--lighter", light.lighten(10).toHexString())
  
document.documentElement.style.setProperty("--dark", dark.toHexString())
document.documentElement.style.setProperty("--darker", dark.darken(10).toHexString())

function pageTo(next)
{
  
  $pager.setAttribute("page", next)
  
  $page = q("page#"+next).scrollTop = 0;
  
  if ( pages.indexOf(next) == -1 )
  {
    pages.push(next);
    emit("pagePushed", next)
  }
}

addEventListener("menuAction", function()
{
  $shell.classList.toggle("nav")
})


addEventListener("pullPageAction", function()
{
  if ( pages.length > 0 ) pages.pop();
  
  if ( pages.length == 0 )
  {
    if (entry == 'index') pages.push('public')
    if (entry == 'admin') pages.push('editor')
    if (entry == 'upload') pages.push('upload')
    if (entry == 'inbox') pages.push('inbox')
  }
    
  var topPage = pages[ pages.length-1 ]
  $pager.setAttribute("page", topPage)
  
})

addEventListener("goBackAction", function()
{
  window.location.back()
})

addEventListener("pagePushed", function(ev)
{
  if ( pages.length > 2 )
  {
    $shell.classList.add("paged")
  }
  
  console.log('pagePushed', ev)
  
  if (ev.originalEvent == "editor")
  {
    $shell.classList.add('nav')
  }
  
})


addEventListener("click", function(ev)
{
  
  if (ev.button == 2) return;
  
  //ev.preventDefault()
  //ev.stopPropagation()
  
  var target = ev.srcElement
  
  console.log('clickhandler ', target)
  
  while( 'hasAttribute' in target )
  {
    
    if (target.hasAttribute("emit"))
    {
      
      var actionName = target.getAttribute("emit")+"Action"
      emit(actionName, ev)
    }
    
    if ( target.hasAttribute("confirm") )
    {
      if( confirm(_("confirmAction")) )
      {
        emit(target.getAttribute("confirm"), ev)
      }
    }
    
    if (target.hasAttribute("link"))
    {
      
      $shell.classList.remove("nav")
      
      let link = target.getAttribute("link")
      
      if ( link.substr(0, 4) == "http" || link.indexOf('.html') > -1 )
      {
        window.location = link
        break; /* only 1 link */
      }
      
      else if ( q("page#"+link) )
      {
        pageTo(link)
      }
      
      else if ( q("#landing ."+link) )
      {
        q("#landing").scrollTop = q("#landing ."+link).offsetTop
      }
      
      else
      {
        UID = link
        emit("loadUID", UID)
      }
      
    }
    
    target = target.parentNode
    
  }

})

$wolf.addEventListener("touchstart", function(ev)
{
  touchStart = new Date().valueOf();
  touchStartX = ev.touches[0].clientX
  touchStartY = ev.touches[0].clientY
  $nav.classList.add("drag")
})

$wolf.addEventListener("touchmove", function(ev)
{
  touchLastX = ev.touches[0].clientX
  
  var xDiff = touchStartX - ev.touches[0].clientX
  var yDiff = touchStartY - ev.touches[0].clientY
  
  if (Math.abs(xDiff) > Math.abs(yDiff))
  {
    ev.preventDefault()
    
    if (!$shell.classList.contains("nav")) var offset = 100-xDiff
    else var offset = -xDiff
    
    if (offset < 0) offset = 0
    //console.log('xDiff', xDiff, 'offset', offset)          
  
    $nav.style.transform = "translateX("+offset+"%)"
    
  }
  
})

$wolf.addEventListener("touchend", function(ev)
{
  if ( ( new Date().valueOf() - touchStart ) < 100 )
  {
    return;
  }
  
  var xDiff = touchStartX - touchLastX
  var offset = 100-xDiff
  
  if (!$shell.classList.contains("nav"))
  {
    if (offset < 50) $shell.classList.add("nav")
    else $shell.classList.remove("nav")
  } else {
    if (offset > 150) $shell.classList.remove("nav") 
    else $shell.classList.add("nav")
  }
  
  $nav.style.transform = null
  $nav.classList.remove("drag")
  
})


