/*
  wearWolf - site generation engine
  Copyright (C) <2018>  <Rodrigo Nishino rodrigo.nsh@gmail.com>
  https://bitbucket.org/rodrigonsh/wearwolf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

var publicVapidKey = "BOD_SHxnNguTNCgbDqwec60A3c7kmHnwup8w2qNeJs_H8lbLBykR5-oMkAqprkkyxUtKzNR5Uc8fI4W3DPGLXAk";
var defaultUID = "aP5sVwfgppRkAnQRj3H7k4FHkeZ2";

var entry = null;
var endPoint = "https://wearwolf-web.firebaseapp.com/";
var www = "wearwolf_web_firebaseapp_com";
var h = window.location.host;
var UID = null;
var publicHosts = null;
var storage = null;
var siteData = null;
var $globalAudio = create("audio");
var showIndex = false;
var $wolf = q("wolf");
var $shell = q("shell");
var $pager = q("pager");
var $nav = q("nav");
var touchStartX = null;
var touchStartY = null;
var touchLastX = null;
var touchStart = null;
var pages = ["init"];
var about = null;
var styles = null;
var loadDEV = ( window.location.search == "?env=dev" );

var light,dark = null;

h = h.replace('www.', '');
h = replace(".", "_", h);
h = replace(":", "_", h);
h = replace("-", "_", h);
