// Embed Youtube Responsively
//https://github.com/jeffehobbs/embedresponsively/blob/master/index.html

addEventListener("set_youtube", function(ev)
{
  
  	var youtubeURL = ev.value
    var youtubeID = null
    
    if (youtubeURL.length == 11)
    {
      youtubeID = youtubeURL
    }
    
		else if (youtubeURL.length > 28){
			var uri = youtubeURL;
			var queryString = {};
			uri.replace(
			    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
			    function($0, $1, $2, $3) { queryString[$1] = $3; }
			);
			youtubeID = (queryString['v']);
		} else {
			youtubeID = youtubeURL.substring(16);
		}
		
    let cont = create("div")
    cont.className = "embed-container"
    
    let iframe = create("iframe")
    iframe.src = "https://www.youtube.com/embed/" + youtubeID
    iframe.setAttribute("frameborder", 0)
    iframe.setAttribute("allowfullscreen", true)

    
    cont.appendChild(iframe)
    
    ev.container.appendChild(cont)
		
})
