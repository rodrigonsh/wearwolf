// google Fonts

var googleFonts = []

addEventListener("updateGoogleFonts", function(ev)
{
  let gFontsLink = q("#google-fonts")
  let fonts = encodeURI(googleFonts.join("|"))
  let addr = "https://fonts.googleapis.com/css?family="
  
  gFontsLink.href = addr+fonts
  
})
