const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.sendInbox = functions.https.onCall((data, context) => 
{
  console.log('sendMessage', data)
  
  const uid = data.to
  
  return new Promise((resol1ve, reject)=>
  {
    
    if ( context.auth.uid == null )
    {
      console.log("context.auth.uid == null");
      reject({'status': "LOGIN"})
      return
    }
    
    
    if ( data.from != context.auth.uid )
    {
      console.error( "FUCKER", data.from, context.auth.uid );
      reject({'status': "LOGIN"})
      return 
    }
    
    
    admin.database()
      .ref(`/users/${uid}/notificationTokens`)
      .once('value', function(s)
      {
        
        console.log('There are', s.numChildren(), 'tokens to send notifications to', uid);
        
        // Notification details.
        const payload = {
          notification: {
            title: 'Nova mensagem no Inbox',
            body: data.message,
            icon: "https://wearwolf-web.firebaseapp.com/img/www.png"
          }
        };

        tokens = Object.keys(s.val());
        return admin.messaging().sendToDevice(tokens, payload);
        
      }).then((response) => {
        
        if ( ! response.results )
        {
          console.error(response);
          return
        }
        
        // For each message check if there was an error.
        const tokensToRemove = [];
        response.results.forEach((result, index) => {
          const error = result.error;
          if (error)
          {
            console.error('Failure sending notification to', tokens[index], error);
            // Cleanup the tokens who are not registered anymore.
            if (error.code === 'messaging/invalid-registration-token' ||
                error.code === 'messaging/registration-token-not-registered')
            {
              tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
            }
          }
          
        });
        
        return Promise.all(tokensToRemove);
        
        
        
        
      })
      .then(function(){ resolve("CARA AQUI TUDO CERTO")  });
      
  })
  
})


exports.manifest = functions.https.onRequest((req, res) => {
  
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET, POST')
  res.set('Content-Type', 'application/manifest+json')
  
  const uid = req.query.uid
  
  return admin.auth().getUser(uid).then(u=>{
    
    admin.database().ref("/sites/"+uid).once("value", function(s)
    {
     
      manifest = 
      {
        "name": "wearWolf",
        "short_name": "wearWolf",
        "start_url": ".",
        "display": "standalone",
        "background_color": "#000",
        "description": "Local website",
        "gcm_sender_id": "103953800507",
        "theme_color": "#0057FF"
      } 
     
      siteData = s.val()
      if (siteData == null) return res.send(manifest)
      
      manifest.name = siteData.about.name
      manifest.short_name = siteData.about.shortName
      
      if ('--surface' in siteData.styles)
      {
        manifest.theme_color = siteData.styles['--surface']
      }      
      
      return res.send(manifest)
      
    })
    
  });
      
  
  
  
});

exports.addToken = functions.https.onCall((data, context) => {

  const uid = context.auth.uid
  const token = data
  
  return new Promise((resolve, reject)=>{
    admin.database()
      .ref(`/users/${uid}/notificationTokens`)
      .once('value', function(s)
      {
        return s.ref.child(token).set(true, function(err){
          if (err) reject(err)
          else resolve({'ok': true})
        })
      })
  })

  
});


exports.onSaveData = functions.database.ref('/sites/{UID}')
    .onWrite((change, context) => {
      
      if (!change.after.exists()) {
        return null;
      }

      // Grab the current value of what was written to the Realtime Database.
      const after = change.after.val();
  
      
      var pub = null
      
      if ('public' in after.about && after.about.public)
      {
        var keys = Object.keys(after)     
        pub = {'about': after.about, 'hero': after[keys[1]]}
        
      }
      
      console.log(context.params.UID, pub)
      
      return admin.database().ref('/public/'+context.params.UID).set(pub)      
      
    });
