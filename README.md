#wearWolf
##Website Factory

Rodrigo Nishino <rodrigo.nsh@gmail.com>

wearWolf é uma engine opensource de construção automatizada de websites 
focados em marketing digital e sites institucionais.

Seu código é essencialmente ecmascript e css minimalistas suficientes
para produzir um site razoável e responsivo a partir de um arquivo de 
configuração em formato JSON

Uma versão em funcionamento deste sistema pode ser vista em:

[https://wearwolf-web.firebaseapp.com]

Lembre-se quando você editar este código fonte não estará editando seu 
site e sim o sistema e todos os sites que são baseados neste sistema

Então tenha cuidado

## Requisitos

* Node [https://nodejs.org]
* Conta no Firebase [https://firebase.google.com/]

## Compilando os Arquivos

    git clone https://bitbucket.org/rodrigonsh/wearwolf.git
    
    cd wearwolf
    
    npm install
    
    
## Executando o servidor 
  
    gulp
    
## Editando os códigos

O gulpfile especifica como os arquivos são montados e processados

As fontes estão em src em seus respectivos diretórios





